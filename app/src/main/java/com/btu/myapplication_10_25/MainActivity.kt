package com.btu.myapplication_10_25

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://ktorhighsteaks.herokuapp.com/")
                .build()

        val service = retrofit.create(ApiService::class.java)

        button.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                val result = service.getResult()
                val productResult = service.getProduct()

                productNameTextView.text = productResult.name
                animalNameTextView.text = result.animal
                Glide.with(imageView).load(result.photo).into(imageView)
            }
        }

    }
}

interface ApiService {
    @GET("animalfacts")
    suspend fun getResult():AnimalFacts

    @POST("GetByBarcode")
    suspend fun getProduct():Product
}

class AnimalFacts(
        val animal:String,
        val photo:String
)

class Product(
        val barcode:String,
        val name:String,
        val price:String,
        val measurement:String
)